package de.vetters88.enums;

public enum Planet
{
	MERKUR	(0.33,		2439 ),
	VENUS	(4.87,		6052 ),
	ERDE	(5.97,		6378 ),
	MARS	(0.64,		3397 ),
	JUPITER	(1898.7,	71493),
	SATURN	(568.51,	60267),
	URANUS	(85.85,		25559),
	NEPTUN	(102.44,	24764);
	
	private final double masse;		// in 10^24 kg
	private final double radius;	// in km
	
	Planet( double mas, double rad )
	{
		this.masse  = mas;
		this.radius = rad;
	}
	
	private double getMasse()
	{
		return this.masse;
	}
	
	private double getRadius()
	{
		return this.radius;
	}
	
	// universale Gravitationskonstante = m * kg^-1 * s^-2
	public static final double G = 6.67300e-11;
	double surfaceGravity()
	{
		return G * this.masse / ( this.radius * this.radius );
	}
	
	double surfaceWeight( double andereMasse )
	{
		return andereMasse * this.surfaceGravity();
	}
}

















