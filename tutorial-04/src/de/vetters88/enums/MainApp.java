package de.vetters88.enums;

public class MainApp
{
	Day tag;
	// Konstruktor der Klasse erwartet einen Aufzählungswert (Enum) aus Day, z.B.: Day.MONDAY
	public MainApp( Day d )		
	{
		this.tag = d;
	}
	
	public void kommentiereDenTag()
	{
		switch( this.tag )
		{
			case MONDAY:	System.out.println("Ich hasse Montag!");
							break;
			case FRIDAY:	System.out.println("Freitage ist der schönste Werktag!");
							break;
			case SATURDAY:	
			case SUNDAY:	System.out.println("Wochenende sind die Besten!");
							break;
			default:		System.out.println("Die anderen Tage sind so lala");
		}
	}
	
	public static void main( String [] args )
	{
		MainApp montag = new MainApp( Day.MONDAY );
		MainApp dienstag = new MainApp( Day.TUESDAY );
		montag.kommentiereDenTag();
		dienstag.kommentiereDenTag();
		System.out.println("");
		
		// ------------- zweites Beispiel - Planeten --------------------
		
		double gewichtErde = 75.0;		// Gewicht eines Menschen auf der Erde
		double masse = gewichtErde / Planet.ERDE.surfaceGravity();
		
		for ( Planet p:	Planet.values() )
		{
			System.out.printf("Das Gewicht auf Planet %s\t betraegt %.2fkg.%n", p, p.surfaceWeight(masse) );
		}
	}
}











