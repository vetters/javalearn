package de.vetters88.nio;

import java.lang.ProcessBuilder;
import java.io.*;

public class MainApp
{
	public static void main( String [] args) throws IOException
	{
		Pfad pf = new Pfad();
		
		pf.erzeuge("C:/Users");
		pf.ausgeben();
		pf.infoPfad1();
		pf.analysieren();
		// pf.erzeugePfad( "C:/Program Files/Git/java/tutorial-09/test.txt" );
		pf.erzeugePfad( "C:/Program Files/Git/java/tutorial-09/anton/berta/caesar/beispiel.txt" );
		
		// Prozess bauen, der den cmd-Befehl tree ... enthält
		ProcessBuilder build_tree = new ProcessBuilder( "cmd", "/c", "tree > tmp000001.txt & type tmp000001.txt & del tmp000001.txt" );
		// unter Linux: // new ProcessBuilder( "bash", "-c", "<command>" );
		
		// Pfadangabe der Ausführung setzen
		build_tree.directory( new File("C:\\Program Files\\Git\\java\\tutorial-09") );
		// Prozess erzeugen und starten
		final Process proc_tree = build_tree.start();
		// Einlesen der Befehlsausgabe, bei Verwendung der Codepage 437
		BufferedReader br = new BufferedReader( new InputStreamReader( proc_tree.getInputStream(), /*proc_tree.getEncoding()*/ "Cp437" ) );
		// zeilenweise ausgeben lassen
		String line = "";
		while( ( line = br.readLine() ) != null )
		{
			System.out.println( line );
		}
	}
}








