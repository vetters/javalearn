package de.vetters88.nio;

import java.nio.file.*;
import java.io.*;

public class Pfad
{
	protected Path pfad1 = Paths.get("C:/Program Files/Git/java/tutorial-09");
	protected Path pfad2;
	
	// erzeugt einen Pfad auf Basis einer String-Variable
	public void erzeuge( String name )
	{
		pfad2 = Paths.get( name );
	}
	
	public void ausgeben()
	{
		System.out.println( "Pfad1: " + pfad1 );
		System.out.println( "Pfad2: " + pfad2 );
		System.out.println( "Zum Trennen der Pfadangaben wird folgendes Zeichen verwendet: " + File.separator );
	}
	
	public void infoPfad1()
	{	// getFileName() liefert den letzten Teil des Pfades
		System.out.println( "getFileName(): " + pfad1.getFileName() );
		
		// getNameCount() liefert die Anzahl der Namensbestandteile
		System.out.println( "getNameCount(): " + pfad1.getNameCount() );
		
		// getParent() liefert das Elternverzeichnis
		System.out.println( "getParent(): " + pfad1.getParent() );
		
		// getRoot() liefert das Wurzelverzeichnis
		System.out.println( "getRoot(): " + pfad1.getRoot() );
	}
	
	public void analysieren()
	{	// Files ist eine Klasse aus dem NIO-Framework
		boolean pfad1_isRegularFile = Files.isRegularFile( pfad1 );
		boolean pfad1_isDirectory   = Files.isDirectory  ( pfad1 );
		boolean pfad1_isReadable    = Files.isReadable   ( pfad1 );
		boolean pfad1_isWritable    = Files.isWritable   ( pfad1 );
		boolean pfad1_isExecutable  = Files.isExecutable ( pfad1 );
		
		System.out.format( "isRegularFile:\t%b%nisDirectory:\t%b%nisReadable:\t%b%nisWritable:\t%b%nisExecutable:\t%b%n", pfad1_isRegularFile, pfad1_isDirectory, pfad1_isReadable, pfad1_isWritable, pfad1_isExecutable );
		
		
	}
	
	public void erzeugePfad( String name )
	{	
		// wir erzeugen uns ein Pfadobjekt
		Path meinPfad = Paths.get( name );
		
		// wir holen uns das Elternverzeichnis
		Path meinVerzeichnis = meinPfad.getParent();
		
		// wir holen uns den Dateinamen
		Path meineDatei = meinPfad.getFileName();
		
		try	// Verzeichnisse erzeugen, falls noch nicht vorhanden
		{
			Files.createDirectories( meinVerzeichnis );
		}
		catch( IOException e )
		{
			System.out.println(e);
		}
		
		
		// pruefen, ob das Verzeichnis existiert
		if( Files.isDirectory( meinVerzeichnis ) )
		{
			try
			{
				// ... dann die Datei anlegen
				Files.createFile( meinPfad );
			}
			catch( IOException e )
			{
				System.out.println(e);
			}
		}
		
		System.out.println( meinPfad );
	}
	
}














