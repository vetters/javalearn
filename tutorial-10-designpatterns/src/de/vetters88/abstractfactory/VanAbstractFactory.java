package de.vetters88.abstractfactory;

public class VanAbstractFactory extends AbstractFactory
{
	// deklariert die Methoden, die fuer das Erzeugen der Produkte zustaendig sind
	// welche Produkte erzeugt werden, haengt dann von den konkreten Fabrik-Implementierungen ab
	
	public Body createBody()
	{
		return new VanBody();
	}
	
	public Chassis createChassis()
	{
		return new VanChassis();
	}
	
	public Windows createWindows()
	{
		return new VanWindows();
	}
	
}