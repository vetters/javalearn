package de.vetters88.abstractfactory;

public abstract class AbstractFactory
{
	// deklariert die Methoden, die fuer das Erzeugen der Produkte zustaendig sind
	// welche Produkte erzeugt werden, haengt dann von den konkreten Fabrik-Implementierungen ab
	
	public abstract Body createBody();
	
	public abstract Chassis createChassis();
	
	public abstract Windows createWindows();
	
}