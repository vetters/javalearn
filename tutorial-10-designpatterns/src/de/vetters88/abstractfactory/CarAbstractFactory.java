package de.vetters88.abstractfactory;

public class CarAbstractFactory extends AbstractFactory
{
	// deklariert die Methoden, die fuer das Erzeugen der Produkte zustaendig sind
	// welche Produkte erzeugt werden, haengt dann von den konkreten Fabrik-Implementierungen ab
	
	public Body createBody()
	{
		return new CarBody();
	}
	
	public Chassis createChassis()
	{
		return new CarChassis();
	}
	
	public Windows createWindows()
	{
		return new CarWindows();
	}
	
}