package de.vetters88.abstractfactory;

public class CarBody implements Body
{
	public String getBodyParts()
	{
		return "Body-shell parts for a car";
	}
}