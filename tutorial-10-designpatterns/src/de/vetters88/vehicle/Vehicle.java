package de.vetters88.vehicle;

import de.vetters88.engine.*;

public interface Vehicle
{
	// Stichwort: nested class
	// Farben dann spaeter verwendbar als Vehicle.Color.<Farbe>;
	public enum Color
	{
		UNPAINTED, BLUE, BLACK, GREEN, RED, SILVER, WHITE, YELLOW
	};
	
	public Engine getEngine();
	
	public Vehicle.Color getColor();
	
	public void paint( Vehicle.Color color );
	
}














