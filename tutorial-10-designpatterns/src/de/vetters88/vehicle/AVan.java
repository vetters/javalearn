package de.vetters88.vehicle;

import de.vetters88.engine.*;

public abstract class AVan extends AVehicle
{
	// Konstruktor
	public AVan( Engine engine )
	{	// klassenintern wird der andere Konstruktor aufgerufen
		this( engine, Vehicle.Color.UNPAINTED );
	}
	
	public AVan( Engine engine, Vehicle.Color color )
	{
		super( engine, color );
	}
	
}














