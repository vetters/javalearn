package de.vetters88.vehicle;

import de.vetters88.engine.*;

public abstract class ACar extends AVehicle
{
	// Konstruktor
	public ACar( Engine engine )
	{	// klassenintern wird der andere Konstruktor aufgerufen
		this( engine, Vehicle.Color.UNPAINTED );
	}
	
	public ACar( Engine engine, Vehicle.Color color )
	{
		super( engine, color );
	}
	
}














