package de.vetters88.vehicle;

import de.vetters88.engine.*;
import de.vetters88.components.*;

public abstract class AVehicle implements Vehicle
{
	// gemeinsame Eigenschaften der Fahrzeuge
	private Engine engine;	
	private Vehicle.Color color;
	private Windows windows;
	
	// Konstruktor
	public AVehicle( Engine engine )
	{	// klassenintern wird der andere Konstruktor aufgerufen
		this( engine, Vehicle.Color.UNPAINTED );
	}
	
	public AVehicle( Engine engine, Vehicle.Color color )
	{
		this.engine = engine;
		this.color  = color;
	}
	
	public AVehicle( Engine engine, Vehicle.Color color, Windows windows )
	{
		this.engine  = engine;
		this.color   = color;
		this.windows = windows;
	}
	
	@Override
	public Engine getEngine()
	{
		return this.engine;
	}
	
	@Override
	public Vehicle.Color getColor()
	{
		return this.color;
	}
	
	public Windows getWindows()
	{
		return this.windows;
	}
	
	@Override
	public void paint( Vehicle.Color color )
	{
		this.color = color;
	}
	
	public void createWindows( Windows windows )
	{
		this.windows = windows;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append( String.format( "%s (%s, %s)", this.getClass().getSimpleName(), this.engine, this.color ) );
		if( this.windows != null )
		{
			sb.append( String.format( "%s", this.windows ) );
		}
		return sb.toString();
	}
}














