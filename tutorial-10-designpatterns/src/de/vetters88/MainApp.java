package de.vetters88;

import de.vetters88.engine.*;
import de.vetters88.vehicle.*;
import de.vetters88.factory.*;
import de.vetters88.builder.*;
import de.vetters88.components.*;
import de.vetters88.abstractfactory.*;

public class MainApp
{
	public static void main( String [] args )
	{
		System.out.println( new Coupe( new TurboEngine(2200), Vehicle.Color.RED ) );
		
		System.out.println( new Pickup( new StandardEngine(3000) ) );
		
		// Fabrik\Fabrikmethode //
		
		AVehicleFactory fab = new CarFactory();
		
		System.out.format( "%s %n", fab.build( AVehicleFactory.DrivingStyle.ECO, Vehicle.Color.WHITE ) );
		System.out.format( "%s %n", fab.build( AVehicleFactory.DrivingStyle.MED, Vehicle.Color.BLUE ) );
		System.out.format( "%s %n", fab.build( AVehicleFactory.DrivingStyle.POWER, Vehicle.Color.RED ) );
		
		// und nun noch einen Van
		fab = new VanFactory();
		
		System.out.format( "%s %n", fab.build( AVehicleFactory.DrivingStyle.ECO, Vehicle.Color.YELLOW ) );
		System.out.format( "%s %n", fab.build( AVehicleFactory.DrivingStyle.MED, Vehicle.Color.GREEN ) );
		System.out.format( "%s %n", fab.build( AVehicleFactory.DrivingStyle.POWER, Vehicle.Color.SILVER ) );
		
		// Variante:
		System.out.format( "%s %n", VehicleFactoryAlternate.make( VehicleFactoryAlternate.Category.CAR,
														 AVehicleFactory.DrivingStyle.POWER,
														 Vehicle.Color.YELLOW ) );
		
		// Erbauer\Builder
		
		// ein Auto im halbfertigen Zustand (inkl. Motor)
		ACar car = new Saloon( new StandardEngine(1300) );
		
		// ein Builderobjekt, dass an das halbfertige Auto zusätzliche Komponenten montiert
		VehicleBuilder builder = new CarBuilder( car );
		
		// ein Director steuert den Produktionsprozess
		VehicleDirector director = new CarDirector();
		
		// das fertige Fahrzeug
		Vehicle v = director.build( builder );
		
		System.out.println( v );
		
		// ein Builderobjekt, dass an den halbfertigen Van zusätzliche Komponenten montiert
		VehicleBuilder vanbuilder = new VanBuilder( new Pickup( new StandardEngine(3000) ) );
		
		// ein Director steuert den Produktionsprozess
		VehicleDirector vandirector = new VanDirector();
		
		// der fertige Van
		Vehicle van = vandirector.build( vanbuilder );
		
		System.out.println( van );
		
		/*		 Abstrakte Fabrik \ abstract factory		*/
		
		// wir erzeugen uns eine Instanz der Car-Abstrakte Fabrik
		AbstractFactory aFab = new CarAbstractFactory();
		
		// wir lassen die Produkte erzeuegen
		System.out.println( aFab.createBody().getBodyParts() );		// fluent interface Technik
		System.out.println( aFab.createChassis().getChassisParts() );
		System.out.println( aFab.createWindows().getWindowsParts() );
		
		// ... und nun der geniale Kniff
		aFab = new VanAbstractFactory();	// andere Fabrik => andere Produkte
		
		System.out.println( aFab.createBody().getBodyParts() );		// fluent interface Technik
		System.out.println( aFab.createChassis().getChassisParts() );
		System.out.println( aFab.createWindows().getWindowsParts() );
		
	}
}



















