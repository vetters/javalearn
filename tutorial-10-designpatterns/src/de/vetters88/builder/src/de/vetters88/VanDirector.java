package de.vetters88.builder;

import de.vetters88.vehicle.*;

public class VanDirector extends VehicleDirector
{
	@Override
	public Vehicle build( VehicleBuilder builder )
	{
		builder.buildBody();
		builder.buildChassis();
		builder.buildWindows();
		builder.buildReinforcedStorageArea();
		
		return builder.getVehicle();
	}	
}

















