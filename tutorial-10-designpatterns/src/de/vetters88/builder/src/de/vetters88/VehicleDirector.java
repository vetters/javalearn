package de.vetters88.builder;

import de.vetters88.vehicle.*;

public abstract class VehicleDirector
{
	// diese Methode soll der Client benutzen
	// sie wird in den konkreten Klassen implementiert
	
	// wir injizieren einen VehicleBuilder	{CarBuilder || VanBuilder}
	public abstract Vehicle build( VehicleBuilder builder );	
}

















