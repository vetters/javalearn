package de.vetters88.builder;

import de.vetters88.vehicle.*;
import de.vetters88.components.*;

public class VanBuilder extends VehicleBuilder
{
	// Die Klasse VanBuilder ueberschreibt jetzt alle Methoden,
	// die für das Erstellen des bestimmten Types von Autos notwendig sind.
	// Die jeweiligen Komponenten erzeugen, alle anderen Methoden verbleiben im Urzustand -> leerer Rumpf
	
	private AVan vanInProgress;
	
	// Konstruktor
	public VanBuilder( AVan van )
	{
		this.vanInProgress = van;
	}
	
	// Liste der Methoden, welche die Komponenten ans Auto "schrauben"
	
	@Override
	public void buildBody()
	{
		// Methode soll hier ein Van-Body-Objekt erstellen
		System.out.println("building van body");
	}
	
	@Override
	public void buildChassis()
	{
		// Methode soll hier ein Van-Chassis-Objekt erstellen
		System.out.println("building van chassis"); 
	}
	
	@Override
	public void buildWindows()
	{
		// // Methode soll hier ein Van-Windows-Objekt erstellen
		System.out.println("building van windows");
		this.vanInProgress.createWindows( new Windows() );
	}
	
	@Override
	public void buildReinforcedStorageArea()
	{
		// // Methode soll hier ein spezielles Van-ReinforcedStorageArea-Objekt erstellen
		System.out.println("building van reinforced storage area");
	}
	
	@Override
	public Vehicle getVehicle()
	{
		return this.vanInProgress;
	}
	
}

















