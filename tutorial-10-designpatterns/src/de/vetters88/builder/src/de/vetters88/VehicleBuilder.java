package de.vetters88.builder;

import de.vetters88.vehicle.*;

public abstract class VehicleBuilder
{
	// Liste der Methoden, die zum Erstellen der Komponenten von Fahrzeugen geeignet sind
	// Die Methoden bekommen einen leeren Rumpf, d.h sie sind nicht mehr nur abstract,
	// sondern schon fertig, aber eben leer
	
	public void buildBody()
	{
		// wird in der konkreten Klasse ueberschrieben
	}
	
	public void buildChassis()
	{
		// wird in der konkreten Klasse ueberschrieben
	}
	
	public void buildWindows()
	{
		// wird in der konkreten Klasse ueberschrieben
	}
	
	public void buildPassengerArea()
	{
		// wird nur in der Klasse CarBuilder ueberschrieben
	}
	
	public void buildReinforcedStorageArea()
	{
		// wird nur in der Klasse VanBuilder ueberschrieben
	}
	
	// abstrakte Methode, die durch den Director aufgerufen wird, um das Fahrzeug auszuliefern
	public abstract Vehicle getVehicle();
	
}

















