package de.vetters88.builder;

import de.vetters88.vehicle.*;
import de.vetters88.components.*;

public class CarBuilder extends VehicleBuilder
{
	// Die Klasse CarBuilder ueberschreibt jetzt alle Methoden,
	// die für das Erstellen des bestimmten Types von Autos notwendig sind.
	// Die jeweiligen Komponenten erzeugen, alle anderen Methoden verbleiben im Urzustand -> leerer Rumpf
	
	private ACar carInProgress;
	
	// Konstruktor
	public CarBuilder( ACar car )
	{
		this.carInProgress = car;
	}
	
	// Liste der Methoden, welche die Komponenten ans Auto "schrauben"
	
	@Override
	public void buildBody()
	{
		// Methode soll hier ein Car-Body-Objekt erstellen
		System.out.println("building car body");
	}
	
	@Override
	public void buildChassis()
	{
		// Methode soll hier ein Car-Chassis-Objekt erstellen
		System.out.println("building car chassis"); 
	}
	
	@Override
	public void buildWindows()
	{
		// // Methode soll hier ein Car-Windows-Objekt erstellen
		System.out.println("building car windows");
		this.carInProgress.createWindows( new Windows() );
	}
	
	@Override
	public void buildPassengerArea()
	{
		// // Methode soll hier ein spezielles Car-PassengerArea-Objekt erstellen
		System.out.println("building car passenger area");
	}
	
	@Override
	public Vehicle getVehicle()
	{
		return this.carInProgress;
	}
	
}

















