package de.vetters88.singleton;

public class SerialNumberGenerator
{
	private static SerialNumberGenerator instance;
	
	// hier mal die Fahrzeugnummer als int-Wert
	private int serial_number = 1;
	
	// wir machen den Konstruktor privat,
	// damit er nicht von außerhalb der Klasse aufgerufen werden kann
	private SerialNumberGenerator()
	{
		/* hier gibt es noch was zu tu'n" */
	}
	
	// diese Methode bekommt die Kontrolle ueber die Objekterzeugung und
	// liefert die *einzige* Instanz dieser Klasse aus
	public static SerialNumberGenerator getInstance()
	{	
		// pruefe, ob bereits eine Instanz der Klasse existiert oder nicht
		if( instance == null )
		{	
			// falls nicht, dann wird eine neue Instanz erzeugt
			instance = new SerialNumberGenerator();
		}
		return instance;	// liefert die Instanz der Klasse als Rueckgabewert aus
	}
	
	@Override
	protected Object clone()
	{
		/* hier gibt es noch was zu tu'n" */
		return instance;
	}
	
	// diese Methode gehoert nicht zum Pattern
	// die Methode liefert eine eindeutige Seriennummer
	public int getNextSerial()
	{
		return serial_number++;
	}
	
}

















