package de.vetters88.singleton;

public enum EnumSerialNumberGenerator
{
	// seit JDK 1.5 lässt sich das Singleton sehr elegant mit Enumerationen realisieren
	// Enums sind von Haus aus Singletons
	
	// ueber die Aufzaehlung kann ich bei Bedarf das Singleton als Multiton verwenden,
	// d.h. ich habe fuer Vehicle und Engine eigene Seriennummern
	VEHICLE, ENGINE;
	
	// hier mal die Fahrzeugnummer als int-Wert
	private int serial_number = 1;
	
	// diese Methode gehoert nicht zum Pattern
	// die Methode liefert eine eindeutige Seriennummer
	public int getNextSerial()
	{
		return serial_number++;
	}
	
}

















