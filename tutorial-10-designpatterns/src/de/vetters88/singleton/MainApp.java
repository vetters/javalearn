package de.vetters88.singleton;

import de.vetters88.engine.*;
import de.vetters88.vehicle.*;

// import de.vetters88.singleton.*;

public class MainApp
{
	public static void main( String [] args )
	{
		// diese Methode ist die einzige Moeglichkeit, an die Singleton-Instanz zu gelangen
		SerialNumberGenerator sng = SerialNumberGenerator.getInstance();
		
		// stresstest
		SerialNumberGenerator sngNotACopy = SerialNumberGenerator.getInstance();
		
		System.out.format( "Seriennummer: %04d %n", sng.getNextSerial() );
		System.out.format( "Seriennummer: %04d %n", sngNotACopy.getNextSerial() );
		System.out.format( "Seriennummer: %04d %n", sng.getNextSerial() );
		System.out.format( "Seriennummer: %04d %n", sngNotACopy.getNextSerial() );
		System.out.format( "Seriennummer: %04d %n", sng.getNextSerial() );
		System.out.format( "Seriennummer: %04d %n", sngNotACopy.getNextSerial() );
		
		System.out.format( "sng: %d, sngNotACopy: %d %n", sng.hashCode(), sngNotACopy.hashCode() );
		
		// hier der Test des EnumSingletons
		System.out.format( "V-§eriennummer: %04d %n", EnumSerialNumberGenerator.VEHICLE.getNextSerial() );
		System.out.format( "E-$eriennummer: %04d %n", EnumSerialNumberGenerator.ENGINE.getNextSerial() );
		System.out.format( "V-§eriennummer: %04d %n", EnumSerialNumberGenerator.VEHICLE.getNextSerial() );
		System.out.format( "E-$eriennummer: %04d %n", EnumSerialNumberGenerator.ENGINE.getNextSerial() );
		System.out.format( "V-§eriennummer: %04d %n", EnumSerialNumberGenerator.VEHICLE.getNextSerial() );
		System.out.format( "E-$eriennummer: %04d %n", EnumSerialNumberGenerator.ENGINE.getNextSerial() );
	}
}