package de.vetters88.engine;

public interface Engine
{
	// liefert den Hubraum
	public int getSize();
	
	// liefert true, falls es sich um einen Turbomotor handelt
	public boolean isTurbo();
}