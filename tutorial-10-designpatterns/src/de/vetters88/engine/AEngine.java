package de.vetters88.engine;

public abstract class AEngine implements Engine
{
	// gemeinsame Eigenschaften aller Motoren
	private int size;
	private boolean turbo;
	
	public AEngine( int size, boolean turbo )
	{
		this.size  = size;
		this.turbo = turbo;
	}
	
	@Override
	public int getSize()
	{
		return this.size;
	}
	
	@Override
	public boolean isTurbo()
	{
		return this.turbo;
	}
	
	@Override
	public String toString()
	{
		return String.format( "%s%d", this.getClass().getSimpleName(), this.size );
	}
}

















