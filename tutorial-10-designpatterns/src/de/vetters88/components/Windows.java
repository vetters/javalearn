package de.vetters88.components;

public class Windows
{
	private int amountOfWindows;
	
	public Windows()
	{
		this.amountOfWindows = 3;
	}
	
	public Windows( int aow )
	{
		this.amountOfWindows = aow;
	}
	
	
	@Override
	public String toString()
	{
		return "[" + this.getClass().getSimpleName() + ": " + amountOfWindows + "]";
	}
}