package de.vetters88.factory;

import de.vetters88.vehicle.*;
import de.vetters88.engine.*;

/***
 * Eine sehr einfache Implementierung einer Fabrikmethode
 ***/
 
public class SimpleFactoryMethod
{	
	// die Fabrikmethode liefert ein Vehicle passend zum (Produkt-)Code
	public static Vehicle produziere( int code )
	{
		switch( code )
		{
			case 1:		return new Saloon( new StandardEngine(1300) );
			case 2:		return new Coupe(  new StandardEngine(1800) );
			case 3:		return new Sport(  new TurboEngine   (2000) );
			default:	return new Coupe(  new StandardEngine(1000) );
		}
	}
}
















