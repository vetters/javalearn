package de.vetters88.factory;

import de.vetters88.vehicle.*;
import de.vetters88.engine.*;

public abstract class AVehicleFactory
{
	// der Kunde entscheidet sich für einen Fahrstil
	public enum DrivingStyle
	{
		ECO, MED, POWER
	};
	
	// diese Methode benutzt der "Kunde", um sein Fahrzeug bauen zu lassen
	public Vehicle build( DrivingStyle style, Vehicle.Color color )
	{
		// hier erfolgt der Aufruf der Fabrikmethode
		Vehicle v = selectVehicle( style );
		
		// das Fahrzeug wird lackiert
		v.paint( color );
		
		// das Fahrzeug wird an den Kunden ausgeliefert
		return v;
	}
	
	// das hier ist die eigentliche Fabrikmethode, die durch die abgeleiteten Fabrikklassen
	// noch implementiert werden muessen
	protected abstract Vehicle selectVehicle( DrivingStyle style );
	
}
















