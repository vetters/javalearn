package de.vetters88.factory;

import de.vetters88.vehicle.*;
import de.vetters88.engine.*;

public abstract class VehicleFactoryAlternate
{
	// 
	public enum Category
	{
		CAR, VAN
	};
	
	// eine statische Methode zum erzeugen der Produkte
	public static Vehicle make( Category cat, AVehicleFactory.DrivingStyle style, Vehicle.Color color )
	{
		AVehicleFactory fab = null;
		
		switch( cat )
		{
			case CAR:	fab = new CarFactory();
						break;
			case VAN:	fab = new VanFactory();
						break;
		}
		
		// das Fahrzeug wird an den Kunden ausgeliefert
		return fab.build( style, color );
	}
	
}
















