package de.vetters88.factory;

import de.vetters88.vehicle.*;
import de.vetters88.engine.*;

public class CarFactory extends AVehicleFactory
{	
	@Override
	protected Vehicle selectVehicle( DrivingStyle style )
	{
		Vehicle v = null;
		switch( style )
		{
			case ECO:	v = new Saloon( new StandardEngine(1300) );
						break;
			case MED:	v = new Coupe ( new StandardEngine(1800) );
						break;
			case POWER:	v = new Sport ( new TurboEngine   (2000) );
						break;
			default:	new Coupe ( new StandardEngine(1000) );
		}
		return v;
	}
	

}
















