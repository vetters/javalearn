package de.vetters88.factory;

import de.vetters88.vehicle.*;
import de.vetters88.engine.*;

public class VanFactory extends AVehicleFactory
{	
	@Override
	protected Vehicle selectVehicle( DrivingStyle style )
	{
		Vehicle v = null;
		switch( style )
		{
			case ECO:	v = new BoxVan( new StandardEngine(1300) );
						break;
			case MED:	v = new Pickup ( new StandardEngine(2200) );
						break;
			case POWER:	v = new BoxVan ( new TurboEngine   (2500) );
						break;
			default:	v = new Pickup ( new StandardEngine(1000) );
		}
		return v;
	}
}
















