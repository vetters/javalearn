package de.vetters88.generics;

public class MainApp
{
	public static void main( String [] args )
	{
		// wir erzeugen eine Box
		Box b = new Box();
		b.add(new String("Hallo, ") );
		System.out.format("Der Inhalt der Box ist: %n%s", b.getContent() );
		
		Box b2 = new Box();
		b2.add(new Car() );
		System.out.format("Der Inhalt der Box ist: %n%s", b2.getContent() );
	}
}














