package de.vetters88.generics;

public class Car
{
	private int milage = 0;
	
	public void move( int km)
	{
		this.milage += km;
	}
	
	@Override
	public String toString()
	{
		return String.format("Ich bin ein %s und habe %dkm auf dem Tacho.", this.getClass().getSimpleName(), this.milage);
	}
}














