package de.vetters88.carpark;

import de.vetters88.vehicle.Car;

public class Garage
{
	Car [] parkingSpace;
	
	public Garage( int count )
	{
		this.parkingSpace = new Car[count];
	}
	
	public void addCar( Car c )
	{
		boolean isParked = false;
		for (int i=0; (i<parkingSpace.length ) && ( ! isParked) ; i++ )
		{
			if ( parkingSpace[i] == null )
			{
				parkingSpace[i] = c;
				isParked = true;
			}
		}
	}
	
	public void writeList()
	{
		for (int i=0; (i<parkingSpace.length ); i++ )
		{
			System.out.println("[" + (i+1) + "] -> " + parkingSpace[i] );		
		}
	}
}