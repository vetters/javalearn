package de.vetters88;

import de.vetters88.vehicle.*;
import de.vetters88.carpark.*;

class MainApp
{
	public static void main(String [] args)
	{
		Greeter greeter = new Greeter();
		greeter.sayHello();
		Car lightningMcQueen = new Car();
		Car Sally = new Car();
		Garage gar = new Garage( 5 );
		gar.addCar( lightningMcQueen );
		gar.addCar( Sally );
		gar.writeList();
	}
}