package de.vetters88.paketB;

import de.vetters88.paketA.*;

public class KlasseC extends KlasseA
{
	public void schauenWirMal()
	{
		System.out.println(this.bloedeIdee);
		System.out.println(this.bessereIdee);
		System.out.println(this.besteIdee);
		System.out.println(this.isMindStomed);
		
		(new KlasseA() ).sendMessage();
	}
}