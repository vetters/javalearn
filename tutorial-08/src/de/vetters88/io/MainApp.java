package de.vetters88.io;

import java.io.IOException;

public class MainApp
{
	public static void main( String [] args) throws IOException
	{
		System.out.println( "[0]=vehicle.txt\n[1]=CopyByteStream\n[2]=CopyCharacterStream\n[3]=CopyLineStream\n[4]=CopyLinesStream\n[5]=CopyScanner\n" );
		switch(args[2])
		{
			case "0":	CopySplitLineStream csls = new CopySplitLineStream();
						csls.copy( args[0], args[1], args[3] );
						break;
			case "1":	CopyByteStream cbs = new CopyByteStream();
						cbs.copy( args[0], args[1] );
						break;
			case "2":	CopyCharacterStream ccs = new CopyCharacterStream();
						ccs.copy( args[0], args[1] );
						break;
			case "3":	CopyLineStream cls = new CopyLineStream();
						cls.copy( args[0], args[1] );
						break;
			case "4":	/*CopyLinesStream clss = new CopyLinesStream();
						clss.copy( args[0], args[1] );*/
						break;
			case "5":	CopyScanner csc = new CopyScanner();
						csc.copy( args[0] );
						break;
			default:	System.out.println("falsches Argument.");
		}
		
	}
}








