package de.vetters88.io;

import java.io.*;
import java.util.Scanner;

public class CopyScanner
{
	public void copy( String input ) throws IOException
	{
		Scanner scan  = null;

		try
		{
			scan = new Scanner( new BufferedReader( new FileReader( input ) ) );		
			while( scan.hasNext() )	
			{	// wir holen uns das naechste Token aus dem Stream
				System.out.println( scan.next() );
			}
		}
		catch( FileNotFoundException e )
		{
			System.out.println( e );
		}
		finally
		{	// ggfs. wirft close() die IOException
			if( scan != null )
			{	// der Aufruf close() wird an das injizierte BufferedReader-Objekt deligiert
				scan.close();
			}
		}
	}
}














