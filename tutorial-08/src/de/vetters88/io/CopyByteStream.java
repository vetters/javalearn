package de.vetters88.io;

import java.io.*;

public class CopyByteStream
{
	public void copy( String input, String output ) throws IOException
	{
		FileInputStream in   = null;
		FileOutputStream out = null;
		try
		{	// ggfs. wird eine FileNotFoundException geworfen
			in  = new FileInputStream ( input  );
			out = new FileOutputStream( output );
			int c;
			while( ( c=in.read() ) != -1 )
			{	// wir schreiben das eingelesene Byte in den FileOutputStream
				out.write(c);
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println( e );
		}
		finally
		{	// close() wird ggfs. eine IOException
			if( in != null )
			{
				in.close();
			}
			if( out != null )
			{
				out.close();
			}
		}
	}
}








