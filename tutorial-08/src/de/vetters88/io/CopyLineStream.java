package de.vetters88.io;

import java.io.*;

public class CopyLineStream
{
	public void copy( String input, String output ) throws IOException
	{
		BufferedReader in  = null;
		PrintWriter    out = null;
		try
		{	// wir erzeugen zunaechst die FileReader- und FileWriter-Objekte
			// und injizieren diese in die gepufferten Klassen BufferedReader und PrintWriter
			// (dependency injection)
			in  = new BufferedReader( new FileReader(input ) );
			out = new PrintWriter   ( new FileWriter(output) );
			
			String line; 
			for(int nummer=1; ( line=in.readLine() ) != null; nummer++ )
			{	// readLine() liefert eine Zeichenkette
				out.println( nummer + ": " + line );
			}
		}
		catch( FileNotFoundException e )
		{
			System.out.println( e );
		}
		finally
		{	// ggfs. wirft close() die IOException
			if( in != null )
			{
				in.close();
			}
			if( out != null )
			{
				out.close();
			}
		}
	}
}














