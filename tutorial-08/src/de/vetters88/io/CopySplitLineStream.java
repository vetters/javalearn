package de.vetters88.io;

import java.io.*;

public class CopySplitLineStream
{
	public void copy( String input, String output, String regex ) throws IOException
	{
		BufferedReader in  = null;
		PrintWriter    out = null;
		try
		{	// wir erzeugen zunaechst die FileReader- und FileWriter-Objekte
			// und injizieren diese in die gepufferten Klassen BufferedReader und PrintWriter
			// (dependency injection)
			in  = new BufferedReader( new FileReader(input ) );
			out = new PrintWriter   ( new FileWriter(output) );
			
			System.out.format("|%14s|%14s|%14s|%14s|%n", "Hersteller", "Farbe", "km-Stand", "Leistung");
			System.out.format("|--------------|--------------|--------------|--------------|%n");
			
			String line; 
			for(int zeile=1; ( line=in.readLine() ) != null; zeile++ )
			{	// readLine() liefert eine Zeichenkette
				String[] tmp = line.split( regex );
				for( String k: tmp )
				{
					System.out.format("|%14s", k );
				}
				System.out.print("|");
				System.out.println();
			}
		}
		catch( FileNotFoundException e )
		{
			System.out.println( e );
		}
		finally
		{	// ggfs. wirft close() die IOException
			if( in != null )
			{
				in.close();
			}
			if( out != null )
			{
				out.close();
			}
		}
	}
}














