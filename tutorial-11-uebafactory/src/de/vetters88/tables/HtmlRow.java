package de.vetters88.tables;

/***
 *
 ***/
 
public class HtmlRow extends ARow
{
	// 
	@Override
	public void display()
	{
		// Html-Tag fuer den Beginn der Tabellenzeile
		System.out.format( "\t%s%n", "<tr>" );
		
		// fuer jede Zelle der Zeile
		for( ACell cell: this.cells )
		{
			cell.display();		// .. lassen wir die naechste Html-Zelle ausgeben
		}
		
		
		// Html-Tag fuer das Ende einer Tabellezeile
		System.out.format( "\t%s%n", "</tr>" );
	}
}




















