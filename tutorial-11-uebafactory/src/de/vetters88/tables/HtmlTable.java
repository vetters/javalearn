package de.vetters88.tables;

/***
 *
 ***/
 
public class HtmlTable extends ATable
{
	// 
	@Override
	public void display()
	{
		// Html-Tag fuer den Beginn der Tabelle
		System.out.format( "%s%n", "<table>" );
		
		// fuer jede Zeile der Tabelle
		for( ARow row: this.rows )
		{
			row.display();		// .. lassen wir die naechste Html-Zeile ausgeben
		}
		
		
		// Html-Tag fuer das Ende einer Tabelle
		System.out.format( "%s%n", "</table>" );
	}
}




















