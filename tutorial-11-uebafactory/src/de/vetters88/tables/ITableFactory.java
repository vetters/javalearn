package de.vetters88.tables;

import java.util.*;

/***
 *
 ***/
 
public interface ITableFactory
{
	public ATable createTable();
	
	public ARow createRow();
	
	public ACell createCell( String content );
}




















