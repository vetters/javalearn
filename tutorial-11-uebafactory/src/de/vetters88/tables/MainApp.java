package de.vetters88;

import de.vetters88.tables.*;

public class MainApp
{
	public static void main( String [] args )
	{
		String[][] data = new String[][]
		{
			{	"z1s1", "z1s2", "z1s3", "z1s4"	},
			{	"z2s1", "z2s2", "z2s3", "z2s4" 	},
			{	"z3s1", "z3s2", "z3s3", "z3s4" 	}
		};
		
		// wir erzeugen uns eine Instanz der ExampleTable-Klasse (Director)
		ExampleTable bsp = new ExampleTable( new HtmlFactory() );
		
		// wir rufen die Ausgabe der Tabelle auf.
		// (unser Director-Objekt ruft createTable, createRow, createCell auf)
		bsp.showTable( data );
		
	}
}

















