package de.vetters88.tables;

/***
 *
 ***/
 
public class HtmlCell extends ACell
{
	public HtmlCell( String c )
	{
		super( c );
	}
	
	// 
	@Override
	public void display()
	{
		// Html-Tag fuer den Beginn der Zelle
		System.out.format( "\t%s", "<td>" );
		
		// wir lassen die Html-Zelleninhalt anzeigen
		System.out.format( "%s", this.content );	
		
		// Html-Tag fuer das Ende einer Tabellezelle
		System.out.format( "%s%n", "</td>" );
	}
}




















