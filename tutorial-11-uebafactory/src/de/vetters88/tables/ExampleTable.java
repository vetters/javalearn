package de.vetters88;

import de.vetters88.tables.*;

/***
 *
 ***/
 
public class ExampleTable
{
	protected ITableFactory tableFactory;
	
	// ueber den Konstruktor injizieren wir eine konkrete Tabellenfabrik
	public ExampleTable( ITableFactory itf )
	{
		this.tableFactory = itf;
	}
	
	// wir uebergeben ein zweidimensionales Feld aus Zeichenketten
	public void showTable( String[][] data )
	{
		// wir lassen die injizierte Fabrik eine Tabelle erzeugen
		ATable table = this.tableFactory.createTable();
		
		// wir verarbeiten die beiden Dimensionen des Arrays
		
		// alle Zeilen verarbeiten
		for( String[] line: data )
		{
			ARow row = this.tableFactory.createRow();
			table.addRow( row );
			
			// alle Spalten verarbeiten
			for( String content: line )
			{
				ACell cell = this.tableFactory.createCell( content );
				row.addCell( cell );
			}
		}
		
		// ..und die Tabelle ausgeben
		table.display();
	}
}