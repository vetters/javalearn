package de.vetters88.tables;

import java.util.*;

/***
 *	Die Klasse ARow kapselt die Spalten einer Tabelle und die Methode,
 *	um neue Zellen zur Tabelle hinzuzufuegen
 *
 *	Die 'fertigen' Teile der Klasse ARow sind fuer alle konkreten Produktklassen gleich.
 *	Einzig die Ausgabe der Tabelle unterscheidet sich.
 *
 ***/
 
public abstract class ARow
{
	// eine Zeile hat mehrere Zellen
	protected List<ACell> cells;
	
	// im Konstruktor der Klasse wird eine leere ArrayList erzeugt
	public ARow()
	{
		this.cells = new ArrayList<ACell>();
		// this.cells = new ArrayList();		// das selbe
	}
	
	// die Methode fuegt eine neue Zelle zur Zeile hinzu
	public void addCell( ACell cell )
	{
		this.cells.add( cell );
	}
	
	// die abstrakte Methode muss in den konkreten Produktklassen
	// (Text-, html-, FileTable) ueberschrieben werden
	public abstract void display();
}




















