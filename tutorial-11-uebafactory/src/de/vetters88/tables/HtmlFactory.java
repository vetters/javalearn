package de.vetters88.tables;

import java.util.*;

/***
 *
 ***/
 
public class HtmlFactory implements ITableFactory
{
	@Override
	public ATable createTable()
	{
		return new HtmlTable();
	}
	
	@Override
	public ARow createRow()
	{
		return new HtmlRow();
	}
	
	@Override
	public ACell createCell( String content )
	{
		return new HtmlCell( content );
	}
}




















