package de.vetters88.tables;


/***
 *	Die Klasse ACell kapselt die Zellen einer Tabelle und
 *	weist den neuen Inhalt einer Zelle hinzu
 *
 *	Die 'fertigen' Teile der Klasse ACell sind für alle konkreten Produktklassen gleich.
 *	Einzig die Ausgabe der Tabelle unterscheidet sich.
 *
 ***/
 
public abstract class ACell
{
	// der Zelleninhalt
	protected String content;
	
	// der Konstruktor setzt den Inhalt der Zelle
	public ACell( String c )
	{
		this.content = c;
	}
	
	// die abstrakte Methode muss in den konkreten Produktklassen
	// (Text-, html-, FileTable) ueberschrieben werden
	public abstract void display();
}




















