package de.vetters88.tables;

import java.util.*;

/***
 *	Die Klasse ATable kapselt die Zeilen einer Tabelle und die Methode,
 *	um neue Zeilen zur Tabelle hinzuzufuegen
 *
 *	Die 'fertigen' Teile der Klasse ATable sind fuer alle konkreten Produktklassen gleich.
 *	Einzig die Ausgabe der Tabelle unterscheidet sich.
 *
 ***/
 
public abstract class ATable
{
	// eine Tabelle besteht aus Zeilen
	protected List<ARow> rows;
	
	// im Konstruktor der Klasse wird eine leere ArrayList erzeugt
	public ATable()
	{
		this.rows = new ArrayList<ARow>();
		// this.rows = new ArrayList();		// das selbe
	}
	
	// fuegt eine neue Zeile zur Tabelle hinzu
	public void addRow( ARow row )
	{
		this.rows.add( row );
	}
	
	// die abstrakte Methode muss in den konkreten Produktklassen
	// (Text-, html-, FileTable) ueberschrieben werden
	public abstract void display();
}




















