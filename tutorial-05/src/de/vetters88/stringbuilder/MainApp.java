package de.vetters88.stringbuilder;

public class MainApp
{
	public static void main( String [] args)
	{
		StringBuilder sbA = new StringBuilder();
		StringBuilder sbB = new StringBuilder("Hallo");
		StringBuilder sbC = new StringBuilder(100);
		
		System.out.format("sbA: \"%s\" %d%n", sbA, sbA.capacity() );
		System.out.format("sbB: \"%s\" %d%n", sbB, sbB.capacity() );
		System.out.format("sbC: \"%s\" %d%n", sbC, sbC.capacity() );
		
		sbA.append( true );
		System.out.format("sbA: \"%s\" %d%n", sbA, sbA.capacity() );
		
		sbB.append( " Welt!" );
		System.out.format("sbB: \"%s\" %d%n", sbB, sbB.capacity() );
		
		sbC.append( sbB );
		System.out.format("sbC: \"%s\" %d%n", sbC, sbC.capacity() );
		
		sbA.insert( 2, false );
		System.out.format("sbA: \"%s\" %d%n", sbA, sbA.capacity() );
		
		sbB.insert( 5, " schoene" );
		System.out.format("sbB: \"%s\" %d%n", sbB, sbB.capacity() );
		
		sbC.insert( 0, sbB );
		System.out.format("sbC: \"%s\" %d%n", sbC, sbC.capacity() );
		
		sbB.replace( 6, 13, "friedliche");
		System.out.format("sbB: \"%s\" %d%n", sbB, sbB.capacity() );
		
		StringBuilder paly = new StringBuilder("Lolli");
		StringBuilder re_paly = new StringBuilder( paly.reverse() );
		paly.append( re_paly.reverse() );
		System.out.format("paly: \"%s\" %d%n", paly, paly.capacity() );
		
		
	}
}














