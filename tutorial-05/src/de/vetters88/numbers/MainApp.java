package de.vetters88.numbers;

import java.util.Locale;
import java.util.Calendar;

public class MainApp
{
	public static void main( String [] args)
	{
		Integer zahlA = new Integer(-1000);
		Integer zahlB = new Integer("4321");
		
		System.out.println( zahlA + ", " + zahlB );
		
		// Zahl aus Zeichenkette dekodieren lassen
		Integer hexA = Integer.decode("0xA0");
		Integer hexB = Integer.decode("0xEF");
		Integer hexC = Integer.decode("#FF" );
		
		Integer octA = Integer.decode("010");
		
		System.out.println( "0xA0, 0XEF, #FF, 010 .. " + hexA + ", " + hexB + ", " + hexC + ", " + octA );
		
		/* wir provozieren die NumberFormatException */
		// Integer nfe = Integer.decode("LBS");
		
		// Zahl aus einer Zeichenkette ermitteln. Die Basis(radix) wird als 2ter parameter angegeben.
		Integer binA = Integer.valueOf("1001", 2);
		Integer octB = Integer.valueOf("1001", 8);
		Integer decC = Integer.valueOf("1001", 10);
		Integer hexD = Integer.valueOf("1001", 16);

		System.out.println( "1001 .. " + binA + ", " + octB + ", " + decC + ", " + hexD);
		
		// wir geben eine Integerzahl als String aus
		int x = 100;
		
		System.out.println( x + ": " + Integer.toBinaryString(x));
		System.out.println( x + ": " + Integer.toHexString(x)	);
		System.out.println( x + ": " + Integer.toOctalString(x)	);
		
		System.out.println( x + ": " + Integer.toString( x, 2) );
		System.out.println( x + ": " + Integer.toString( x, 8) );
		System.out.println( x + ": " + Integer.toString( x, 10) );
		System.out.println( x + ": " + Integer.toString( x, 16) );
		
		// Ausgaben formatieren
		long zahl1 = 543321;
		System.out.format( "Zahl: %d %n", zahl1 );
		System.out.format( "Zahl: %08d %n", zahl1 );
		System.out.format( "Zahl: %-8d %n", zahl1 );
		
		double pi = Math.PI;
		
		System.out.format( "Die Kreiszahl: %f %n", pi);
		System.out.format( "Die Kreiszahl: %.3f %n", pi);
		System.out.format( "Die Kreiszahl: %10.3f %n", pi);
		System.out.format( "Die Kreiszahl: %-10.3f %n", pi);
		System.out.format( Locale.FRANCE, "Die Kreiszahl: %-10.3f %n", pi);
		System.out.format( Locale.US, "Die Kreiszahl: %-10.3f %n", pi);
		
		Calendar cal = Calendar.getInstance();
		
		System.out.format( "%te.%tB.%tY %n", cal, cal, cal);
		System.out.format( Locale.FRANCE, "%te.%tB.%tY %n", cal, cal, cal);
		System.out.format( Locale.CHINESE, "%te.%tB.%tY %n", cal, cal, cal);
		
		// explizite Indizierung der Werte
		System.out.format( Locale.FRANCE, "%1$te.%1$tB.%1$tY %n", cal);
		
		System.out.format( "%3$d %1$d %2$d", hexA, hexB, hexC );
	}
}














