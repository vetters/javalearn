package de.vetters88.collections;

import java.util.*;

public class CarSortCmpFarbe implements Comparator<CarSort>
{
	@Override
	public int compare( CarSort c1, CarSort c2 )
	{	// farbe ist String -> implementiert Comparable -> compareTo
		return c1.farbe.compareTo(c2.farbe);	
	}
}