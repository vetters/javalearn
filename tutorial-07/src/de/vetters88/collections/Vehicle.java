package de.vetters88.collections;


interface Vehicle 
{
	public boolean isMaintained=false;
	public void move();
}