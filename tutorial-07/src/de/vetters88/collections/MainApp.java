package de.vetters88.collections;

import java.util.*;
import java.util.concurrent.*;

public class MainApp
{
	public static void main( String [] args )
	{
		List<String> list = new ArrayList<String>();		// erzeugt eine neue, leere ArrayList
		
		list.add("Hallo");
		list.add(new String("Welt!"));
		list.add("eins");
		list.add("eins");
		
		// erzeugt eine neue Liste mit den Elementen der vorhandenen Liste:
		List<String> andereListe = new ArrayList<String>( list );
		// erzeugt eine neue Liste mit der angegebenen Kapazitaet:
		List<String> nochEineListe = new ArrayList<String>( 1000 );
		
		System.out.format("list[%d], andereListe[%d], nochEineListe[%d]%n", list.size(), andereListe.size(), nochEineListe.size() );
		
		andereListe.add("wir");
		andereListe.add("brauchen");
		andereListe.add("eine Main-Methode");
		
		System.out.format("Die \"list\" ist in der \"andereListe\" enthalten?: %b %n", andereListe.containsAll( list ) );
		
		for( String s:	andereListe )
		{
			System.out.println( s );
		}
	
		String tmp_String = andereListe.remove(2);		// entfernt ein Element aus der Liste
		
		for( String s:	andereListe )
		{
			System.out.println( s );
		}
	
		System.out.println("Entfernt wurde: " + tmp_String + "\n");
		
		Car a1 = new Car();
		Car a2 = new Car();
		
		System.out.format("a1==a2 (%b)%n", a1.equals(a2) );
		System.out.format("a1==a2 (%b)%n", a1.hashCode() == a2.hashCode() );
		
		Car a3 = new Car(0, "Blau", 120, "Opel");
		
		System.out.format("a1==a3 (%b)%n", a1.equals(a3) );
		System.out.format("a1==a3 (%b)%n", a1.hashCode() == a3.hashCode() );
		
		Set<Car> carSet = new TreeSet<Car>();
		
		carSet.add( a1 );
		carSet.add( a2 );
		carSet.add( a3 );
		
		System.out.format("carSet[%d] %n", carSet.size() );
		
		List<Integer> intListe = new ArrayList<Integer>();
		
		// Elemente hinzufügen (auch Duplikate)
		
		intListe.add(10);
		intListe.add(10);
		intListe.add(10);
		intListe.add(10);
		
		intListe.add(20);
		intListe.add(20);
		intListe.add(20);
		
		intListe.add(30);
		intListe.add(30);
		intListe.add(30);
		intListe.add(30);
		
		System.out.format("Das Objekt 10 befindet sich an der Stelle %d %n", intListe.indexOf(10) );
		System.out.format("Das Objekt 10 kommt zum letzten Mal an der Stelle %d vor. %n", intListe.lastIndexOf(10) );
		
		System.out.println("Das ist eine Subliste von intListe: " + intListe.subList(4, 8) );
		
		Queue<Integer> queue = new LinkedList<Integer>();
		
		for( int i=10; i>=0; i-- )
		{
			queue.add(i);
		}
		
		System.out.println( queue );
		
		
		while( !queue.isEmpty() )
		{
			System.out.println( queue.remove() );	// holt immer das erste Element aus der Warteschlange
			System.out.println( "Das erste Element der queue ist: " + queue.peek() );
		}		
		
		Queue<Integer> arrayQueue = new ArrayBlockingQueue<Integer>(10);
		
		for( int i=0; i<10; i++ )
		{	// gibt die Queue aus und TRUE|FALSE der add()-Operation
			System.out.format( "%s: %b%n", arrayQueue, arrayQueue.add(i) );	
		}
		
		Deque<Integer> deque = new ArrayDeque<Integer>(10);		// eine double-ended queue mit 10 Elementen
		
		System.out.format( "Deque: %s mit %d Elementen%n", deque, deque.size() );
		
		deque.offerFirst(10);
		deque.offerFirst(20);
		deque.offerLast (30);
		deque.offerLast (40);
		
		System.out.format( "Deque: %s mit %d Elementen%n", deque, deque.size() );
		
		deque.addFirst(50);
		deque.addFirst(60);
		deque.addFirst(70);
		deque.addFirst(80);
		
		deque.addLast(90);
		deque.addLast(100);
		
		System.out.format( "Deque: %s mit %d Elementen%n", deque, deque.size() );
		
		deque.addLast(100);
		System.out.format( "%d Elemente%n", deque.size() );
		
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put( 1, "Hallo" );
		map.put( 2, "Welt!" );
		map.put( 3, "Wir reden hier ueber das Leitungswasser" );
		map.put( 3, "Mal schaun'" );
		
		System.out.println( map );
		
		Set<Integer> keys = map.keySet();
		Collection<String> values = map.values();
		
		System.out.format( "%s%n%s%n", keys, values );
		
		System.out.format( "Der Schluessel 3 ist in der Map enthalten?: %b%n", map.containsKey(3) );
		System.out.format( "Der Schluessel 4 ist in der Map enthalten?: %b%n", map.containsKey(4) );
		
		System.out.format( "Der Wert \"Hallo\" ist in der Map enthalten?: %b%n", map.containsValue("Hallo") );
		System.out.format( "Der Wert \"Muttiheft\" ist in der Map enthalten?: %b%n", map.containsValue("Muttiheft") );
		
		List<CarSort> lcs = new ArrayList<CarSort>();
		
		lcs.add( new CarSort() );
		lcs.add( new CarSort("Rot", 5000, 450) );
		lcs.add( new CarSort("Grau", 444, 120) );
		lcs.add( new CarSort("Blau", 10000, 110) );
		lcs.add( new CarSort("Occer", 30000, 180) );
		lcs.add( new CarSort("Weinrot", 50000, 120) );
		lcs.add( new CarSort("Sauerstoffblond", 100, 50) );
		
		System.out.println( "\n" + lcs );
		
		// Wir lassen mit dem Comparator sortieren
		
		Collections.sort( lcs, new CarSortCmpFarbe() );  // ... nach Farbe
		System.out.println( lcs );
		
		Collections.sort( lcs, new CarSortCmpKm() );  // ... nach km_Stand
		System.out.println( lcs );
		
		Collections.sort( lcs, new CarSortCmpPs() );  // ... nach Motorleistung
		System.out.println( lcs );
		
	}
}
















