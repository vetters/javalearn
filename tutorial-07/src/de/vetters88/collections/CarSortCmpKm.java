package de.vetters88.collections;

import java.util.*;

public class CarSortCmpKm implements Comparator<CarSort>
{
	@Override
	public int compare( CarSort c1, CarSort c2 )
	{	// km_Stand ist int -> implementiert Comparable -> compareTo
		return (int)Math.signum( c1.km_Stand - c2.km_Stand );
	}
}