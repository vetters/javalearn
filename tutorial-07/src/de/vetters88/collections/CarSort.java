package de.vetters88.collections;

public class CarSort
{
	protected String farbe;
	protected int km_Stand;
	protected int motorleistung;
	
	CarSort()
	{
		this.farbe = "Transparent";
		this.km_Stand = 0;
		this.motorleistung = 0;
	}
	
	CarSort(String f, int k, int ps)
	{
		this.farbe = f;
		this.km_Stand = k;
		this.motorleistung = ps;
	}
	
	@Override
	public String toString()
	{
		return String.format( "[%s, %d, %d]", this.farbe, this.km_Stand, this.motorleistung );
	}
}