package de.vetters88.collections;

import java.util.*;

public class Car implements Comparable<Car>
{
	private static int id=1;
	
	private		int		km_stand;
	private		String	farbe;
	private		int		motorleistung;
	private		String	hersteller;
	private		int		car_nr;
	
	public Car (int km, String far, int ps, String her)
	{
		this.km_stand = km;
		this.farbe = far;
		this.motorleistung = ps;
		this.hersteller = her;
		this.car_nr = id++;
	}
	
	public Car ()
	{
		this.km_stand = 0;
		this.farbe = "Transparent";
		this.motorleistung = 0;
		this.hersteller = "";
		this.car_nr = id++;
	}
	
	public int getKm_stand()
	{
		return this.km_stand;
	}
	
	public String getFarbe()
	{
		return this.farbe;
	}
	
	public int getMotorleistung()
	{
		return this.motorleistung;
	}
	
	public String getHersteller()
	{
		return this.hersteller;
	}
	
	public int getCar_nr()
	{
		return this.car_nr;
	}
	
	public void setKm_stand(int km)
	{
		this.km_stand = km;
	}
	
	public void setFarbe(String far)
	{
		this.farbe = far;
	}
	
	public void setMotorleistung(int ps)
	{
		this.motorleistung = ps;
	}
	
	public void setHersteller(String her)
	{
		this.hersteller = her;
	}
	
	
	@Override
	public boolean equals(Object o)
	{
		Car tmp = (Car) o;
		if( ( this.getKm_stand() / 5000 ) != ( tmp.getKm_stand() / 5000 ) )
		{
			return false;
		}
		if( this.getFarbe() != tmp.getFarbe() )
		{
			return false;
		}
		if( this.getMotorleistung() != tmp.getMotorleistung() )
		{
			return false;
		}
		if( this.getHersteller() != tmp.getHersteller() )
		{
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode()
	{
		int hash  = ( this.getKm_stand() / 5000 )* 5 + 5;
		hash *= this.getFarbe().hashCode() * 11 + 11;
		hash *= this.getMotorleistung() * 19 + 19;
		hash *= this.getHersteller().hashCode() * 13 + 13;
		return hash;
	}
	
	@Override
	public int compareTo(Car c)
	{
		return this.car_nr - c.car_nr;
	}
}













