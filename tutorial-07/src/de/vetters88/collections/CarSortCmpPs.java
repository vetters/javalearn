package de.vetters88.collections;

import java.util.*;

public class CarSortCmpPs implements Comparator<CarSort>
{
	@Override
	public int compare( CarSort c1, CarSort c2 )
	{	// motorleistung ist int -> implementiert Comparable -> compareTo
		return (int)Math.signum( c1.motorleistung - c2.motorleistung );
	}
}